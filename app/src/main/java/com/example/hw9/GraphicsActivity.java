package com.example.hw9;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class GraphicsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graphics);

        MyGraphics myGraphics = new MyGraphics(GraphicsActivity.this);
        setContentView(myGraphics);
    }
}