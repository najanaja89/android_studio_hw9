package com.example.hw9;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
//import android.widget.Toolbar;
import androidx.appcompat.widget.Toolbar;

import org.w3c.dom.Text;

import java.lang.reflect.Type;
import java.nio.file.StandardOpenOption;

public class MainActivity extends AppCompatActivity {

    Animation animation = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView fontText = findViewById(R.id.fontFromFontRes);
        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView animImageView = findViewById(R.id.animImageView);
        animImageView.setBackgroundResource(R.drawable.frame_animation);
        AnimationDrawable animationDrawable = (AnimationDrawable) animImageView.getBackground();


        //toolbar.inflateMenu(R.menu.mymenu);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.alpha:
                        animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.myalpha);
                        fontText.startAnimation(animation);
                       break;
                    case R.id.rotate:
                        animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.myrotate);
                        fontText.startAnimation(animation);
                        break;

                    case R.id.scale:
                        animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.myscale);
                        fontText.startAnimation(animation);
                        break;

                    case R.id.translate:
                        animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.mytranslate);
                        fontText.startAnimation(animation);
                        break;

                    case R.id.set:
                        animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.myset);
                        fontText.startAnimation(animation);
                        break;

                    case R.id.frame:
                        animationDrawable.start();
                        break;

                }

                return true;
            }
        });

        Button btnGraphics = findViewById(R.id.graphicsBtn);

        btnGraphics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, GraphicsActivity.class));
            }
        });
        TextView fontFromAssets = findViewById(R.id.fontFromAssets);
        fontFromAssets.setTypeface(Typeface.createFromAsset(getAssets(), "MontserratAlternates-Black.otf"));

    }
}