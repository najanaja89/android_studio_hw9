package com.example.hw9;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class MyGraphics extends View {
    private Paint paint;

    public MyGraphics(Context context) {
        super(context);
        paint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLUE);
        canvas.drawPaint(paint);

        drawCircle(canvas);
        drawTextWithRotation(canvas);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_foreground);
        //drawBitmap(canvas, bitmap);
    }
     private void drawCircle(Canvas canvas){
        paint.setAntiAlias(true);
        paint.setColor(Color.YELLOW);
        canvas.drawCircle(950, 30,25, paint);
     }

     private void drawTextWithRotation(Canvas canvas) {
        paint.setColor(Color.GRAY);
        paint.setTextSize(50f);
        int x = 100;
        int y =40;
        canvas.rotate(45, x, y  );
        canvas.drawText("Text with rotation", 700, 700, paint);
     }

     private void drawBitmap(Canvas canvas, Bitmap bitmap){
        canvas.restore();
        canvas.drawBitmap(bitmap, 700, 800, paint);
     }

}
